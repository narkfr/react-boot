import PropTypes from "prop-types";
import React from "react";
import { Well } from "react-bootstrap";
import { translate } from "react-i18next";

import { setLSItem } from "../libs/localStorage";

import "./App.scss";
import Kittens from "../assets/kittens-main_full.jpg";

const App = ({ t, i18n }) => {
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
    setLSItem("language", lng);
  };
  return (
    <Well>
      <h1 className="app__title">{t("hello")}</h1>
      <div>
        <button type="button" onClick={() => changeLanguage("fr")}>
          fr
        </button>
        <button type="button" onClick={() => changeLanguage("en")}>
          en
        </button>
      </div>
      <img src={Kittens} alt="Some very sweet kittens" />
    </Well>
  );
};

App.propTypes = {
  t: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired // eslint-disable-line react/forbid-prop-types
};

export default translate("translations")(App);
