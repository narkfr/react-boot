/* global document */
import React from "react";
import ReactDOM from "react-dom";
import { I18nextProvider } from "react-i18next";

import i18n from "./libs/i18n";

import App from "./components/App";

ReactDOM.render(
  <I18nextProvider i18n={i18n}>
    <div lang={i18n.language}>
      <App />
    </div>
  </I18nextProvider>,
  document.getElementById("app")
);

// Allow hot reload
module.hot.accept();
