/* global navigator */
import i18next from "i18next";
import { getLSItem } from "./localStorage";

import fr from "../../languages/fr.json";
import en from "../../languages/en.json";

i18next.init({
  // Get language from localStorage if the user has changed it
  lng: getLSItem("language") || navigator.language,
  fallbackLng: "en",
  // Using simple hardcoded resources for simple example
  resources: {
    fr,
    en
  },
  // have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  interpolation: {
    escapeValue: false // not needed for react!!
  },

  react: {
    wait: true
  }
});

export default i18next;
