/* global localStorage */
const getLSItem = item => localStorage.getItem(item);

const setLSItem = (item, value) => {
  localStorage.setItem(item, value);
};

const clearLS = () => localStorage.clear();

export { getLSItem, setLSItem, clearLS };
