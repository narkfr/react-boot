const webpack = require("webpack");
const path = require("path");

const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  output: {
    filename: "main.[hash].js",
    path: path.resolve(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.(css|scss)$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: ["file-loader", "image-webpack-loader"]
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin("dist/*.*", {}),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      foo: "bar" // Use <%= htmlWebpackPlugin.options.foo %> to pass variable in html template
    }),
    new MiniCssExtractPlugin({
      filename: "main.[hash].css",
      chunkFilename: "[id].css"
    })
  ],
  devServer: {
    contentBase: "./dist",
    hot: true
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000 // How often check for changes (in milliseconds)
  }
};
