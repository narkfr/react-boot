# React-boot

React-boot is a minimal react setup build with **the tools I like / need** to work with React.js

## Requirements

React-boot us a basic Javascript project, you need basic Javascript tools :

- nodejs >= 10
- yarn (if you want)
- prettier & eslint installed in your IDE

## Features

- [x] Webpack 4.0
- [x] babel
- [x] hot-reload
- [x] React
- [x] bootstrap
- [x] Sass
- [x] hash in exported filename
- [x] i18n
- [] redux & localStorage
- [] react-router
- [] form
- [] Gitlab CI
- [] e2e testing

## Installation

```
git clone git@framagit.org:narkfr/react-boot.git
mkdir react-boot
yarn install
yarn start
```

## Scripts

- `yarn test`: launch tests suite
- `yarn analyse`: generate a stats.json file to analyse on http://webpack.github.io/analyse/

## Lint & prettier

I use `react`, `import`, `jsx-a11y` plugins with eslint.
Prettier is used to format the code, with Airbnb configuration. If you want to use something else, just do it, you'll have to remove related packages and update .eslintrc file
